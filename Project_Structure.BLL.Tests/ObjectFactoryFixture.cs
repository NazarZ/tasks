﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Project_Structure.DAL.Context;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Repositories;

namespace Project_Structure.BLL.Tests
{
    public class ObjectFactoryFixture
    {
        public IMapper Mapper { get; private set; }
        public IUnitOfWork Contex { get; private set; }
        public ObjectFactoryFixture(IMapper mapper)
        {
            //Contex = CreateUnitOfWork();
            Mapper = mapper;
        }
        public EFUnitOfWork CreateUnitOfWork()
        {
            var db = new ProjectDbContext(CreateNewContextOptions());
            return new(db);
        }
        private DbContextOptions<ProjectDbContext> CreateNewContextOptions()
        {
            // Create a fresh service provider, and therefore a fresh 
            // InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<ProjectDbContext>();
            builder.UseInMemoryDatabase("MockDB")
                   .UseInternalServiceProvider(serviceProvider);

            return builder.Options;
        }
    }
}
