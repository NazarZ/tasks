﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.MappingProfiles;
using Project_Structure.BLL.Services;
using Project_Structure.DAL.Context;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Repositories;
using System;
using System.Reflection;

namespace Project_Structure.BLL.Tests
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ProjectDbContext>(contextOptions => contextOptions.UseInMemoryDatabase("ProjectDb"),
                optionsLifetime: ServiceLifetime.Transient);
            services.AddTransient<IUnitOfWork, EFUnitOfWork>();
            services.AddTransient<IProjectService, ProjectService>();
            services.AddTransient<ITaskService, TasksService>();
            services.AddTransient<ITeamService, TeamService>();
            services.AddTransient<IUserService, UserService>();

            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
            });
            Assembly.GetExecutingAssembly();
        }

        //You can add the method parameters to be used, and the services instance will be automatically obtained from the registered services, similar to asp.net  Configure method in core
        public void Configure(IServiceProvider applicationServices)
        {
            //Some test data to be initialized can be put here
            // InitData();
        }
    }
}

