﻿using AutoMapper;
using FakeItEasy;
using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Services;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Xunit;

namespace Project_Structure.BLL.Tests
{
    public class TaskServiceTests : IClassFixture<ObjectFactoryFixture>
    {
        private TasksService _taskService;
        private IUnitOfWork fakeContext;
        private IMapper fakeMapper;
        public TaskServiceTests(ObjectFactoryFixture fixture)
        {
            _taskService = new(fixture.CreateUnitOfWork(), fixture.Mapper);
        }

        [Fact]
        public void GetTaskServiceIsNotNull()
        {
            Assert.NotNull(_taskService);
        }
        [Fact]
        public void CreatedNewTask_Then_CalledCreateAndSaveMethodsOne()
        {
            _taskService = CrateFakeTasksService();

            var fakeTask = A.Fake<TaskDTO>();

            var user = _taskService.CreateTask(fakeTask);

            A.CallTo(() => fakeContext.Tasks.Create(A<Task>._)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeContext.SaveAsync()).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void UpdateddTask_Then_CalledUddateAndSaveMethodsOne()
        {
            _taskService = CrateFakeTasksService();
            var fakeTask = A.Fake<TaskDTO>();

            var user = _taskService.UpdateTask(fakeTask);

            A.CallTo(() => fakeMapper.Map<Task>(A<TaskDTO>._)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeContext.Tasks.Update(A<Task>._)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeContext.SaveAsync()).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async void DeletedTask_Then_CalledDeletedAndSaveMethodsOne()
        {
            _taskService = CrateFakeTasksService();
            ClearRecordedCalls();
            var fakeId = 1;
            await _taskService.DeleteTask(fakeId);

            A.CallTo(() => fakeContext.Tasks.Delete(A<int>._)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeContext.SaveAsync()).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async void GetTaskById_Then_CalledGetMethodOne()
        {
            _taskService = CrateFakeTasksService();
            var fakeId = 1;

            await _taskService.GetTaskById(fakeId);

            A.CallTo(() => fakeContext.Tasks.Get(A<int>._)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async void CreateNewTask_Than_DBContainsNewTask()
        {
            var user = CreateTaskDTO();

            var cratedTask = await _taskService.CreateTask(user);
            var listTask = await _taskService.GetAllTasks();

            Assert.Single(listTask, cratedTask);
            _taskService.Dispose();
        }


        [Theory]
        [InlineData(-100, 1)]
        [InlineData(1, -1)]
        [InlineData(-1, -1)]
        public void CreateNewTask_WhileInvalidProperty_Than_DoesNotPassValidation(int performer, int project)
        {
            var user = new TaskDTO()
            {
                ProjectId = project,
                PerformerId = performer
            };
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(user);
            Assert.False(Validator.TryValidateObject(user, context, results, true));
            _taskService.Dispose();
        }
        [Fact]
        public async void DeleteTask_Than_DbNotContainsNewTask()
        {
            var user = CreateTaskDTO();
            var cratedTask = await _taskService.CreateTask(user);

            await _taskService.DeleteTask(cratedTask.Id);
            var listTask = await _taskService.GetAllTasks();

            Assert.Empty(listTask);
            _taskService.Dispose();
        }
        [Fact]
        public async System.Threading.Tasks.Task DeleteUse_When_TaskDoesNotExist_Than_ArgumentExeption()
        {
            var user = CreateTaskDTO();
            var cratedTask = await _taskService.CreateTask(user);

            await _taskService.DeleteTask(cratedTask.Id);
            await Assert.ThrowsAsync<ArgumentException>(() => _taskService.DeleteTask(cratedTask.Id));
        }


        [Fact]
        public async void UpdateTask_MarkAsFinished_Than_UpdateDB()
        {
            var user = CreateTaskDTO();

            var cratedTask = await _taskService.CreateTask(user);

            var toUpdate = await _taskService.GetTaskById(cratedTask.Id);
            toUpdate.FinishedAt = DateTime.Now;
            var listTask = await _taskService.GetAllTasks();
            var updatedTask = await _taskService.UpdateTask(toUpdate);

            listTask = await _taskService.GetAllTasks();

            Assert.Single(listTask, updatedTask);
            Assert.Equal(updatedTask.FinishedAt, listTask.First().FinishedAt);
        }


        private TaskDTO CreateTaskDTO()
        {
            return new()
            {
                Name = "Task name",
                Description = "TaskDescription",
                State = Binary.Linq.BL.Models.Enum.TaskState.First,
                ProjectId = 1,
                PerformerId = 1,
                CreatedAt = DateTime.Now.AddMonths(-2),
            };
        }


        private void ClearRecordedCalls()
        {
            Fake.ClearRecordedCalls(fakeContext);
            Fake.ClearRecordedCalls(fakeMapper);
        }
        private TasksService CrateFakeTasksService()
        {
            fakeContext = A.Fake<IUnitOfWork>();
            fakeMapper = A.Fake<IMapper>();
            return new(fakeContext, fakeMapper);
        }

        public void Dispose()
        {
            _taskService.Dispose();
        }
    }
}
