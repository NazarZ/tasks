﻿using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Project_Structure.WebAPI.IntegrationTests
{
    public class ProjectsControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory>
    {
        private readonly CustomWebApplicationFactory _customWebApplicationFactory;
        private readonly HttpClient _client;
        private string _requestUri;
        public ProjectsControllerIntegrationTest(CustomWebApplicationFactory customWebApplicationFactory)
        {
            _customWebApplicationFactory = customWebApplicationFactory;
            _client = _customWebApplicationFactory.CreateClient();
            _requestUri = _client.BaseAddress.AbsoluteUri + "api/Projects";
        }
        [Fact]
        public async Task GetProjectResource_HttpResponse_ShouldReturn200OK()
        {
            // Arrange

            // Act
            var sut = await _client.GetAsync(_requestUri);

            // Assert 
            var responseCode = sut.StatusCode;
            Assert.Equal(HttpStatusCode.OK, responseCode);
        }
        [Theory]
        [InlineData("{\"id\": 0,\"authorId\": 1,\"teamId\": 1,\"name\": \"New Project\",\"description\": \"Lorem ipsum\",\"deadline\": \"2021-07-05T12:07:52.366Z\",\"createdAt\": \"2021-07-05T12:07:52.366Z\"}")]

        public async Task CreateNewProject_Then_HttpResponse_ShouldReturn201Created(string jsonString)
        {
            // Arrange

            // Act
            var sut = await _client.PostAsync(_requestUri, new StringContent(jsonString, Encoding.UTF8, "application/json"));

            // Assert 
            var responseCode = sut.StatusCode;
            Assert.Equal(HttpStatusCode.Created, responseCode);
        }
        [Theory]
        [InlineData("{\"id\": 0,\"authorId\": \"1\",\"teamId\": -1,\"name\": \"New Project\",\"description\": \"Lorem ipsum\",\"deadline\": \"2021-07-05T12:07:52.366Z\",\"createdAt\": \"2021-07-05T12:07:52.366Z\"}")]
        [InlineData("{\"id\": 0,\"authorId\": -1,\"teamId\": 1,\"name\": \"New Project2\",\"description\": \"Lorem ipsum\",}")]
        public async Task CreateNewProject_When_IncorrectData_Then_HttpResponse_ShouldReturn400BadRequest(string jsonString)
        {
            // Arrange

            // Act
            var sut = await _client.PostAsync(_requestUri, new StringContent(jsonString, Encoding.UTF8, "application/json"));

            // Assert 
            var responseCode = sut.StatusCode;
            Assert.Equal(HttpStatusCode.BadRequest, responseCode);
        }
    }
}
