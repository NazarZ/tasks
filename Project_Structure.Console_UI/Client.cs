﻿using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Project_Structure.Console_UI
{
    public class Client
    {
        private HttpClient _httpClient;
        JsonSerializerOptions options = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true
        };
        public Client()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri(Config.BaseUri);
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<Dictionary<ProjectModel, int>> GetCountTasksUserInProjects(int userId)
        {
            HttpResponseMessage response = await _httpClient.GetAsync($"/api/Queries/GetCountTasksUserInProjects/{userId}");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                return JsonSerializer.Deserialize<Dictionary<ProjectModel, int>>(i, options);
            }
            return null;
        }

        public async Task<List<FinishedTaskModel>> GetFinishedTaskByUserId(int userId)
        {
            HttpResponseMessage response = await _httpClient.GetAsync($"/api/Queries/GetFinishedTaskByUserId/{userId}");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                return JsonSerializer.Deserialize<List<FinishedTaskModel>>(i, options);
            }
            return null;
        }

        public async Task<Task6Model> GetTask6(int userId)
        {
            HttpResponseMessage response = await _httpClient.GetAsync($"/api/Queries/GetTask6/{userId}");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                return JsonSerializer.Deserialize<Task6Model>(i, options);
            }
            return null;
        }

        public async Task<List<TaskModel>> GetTasksByUserId(int userId)
        {
            HttpResponseMessage response = await _httpClient.GetAsync($"/api/Queries/GetTasksByUserId/{userId}");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                return JsonSerializer.Deserialize<List<TaskModel>>(i, options);
            }
            return null;
        }

        public async Task<List<TeamUsers>> GetTeams()
        {
            HttpResponseMessage response = await _httpClient.GetAsync($"/api/Queries/GetTeams");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                return JsonSerializer.Deserialize<List<TeamUsers>>(i, options);
            }
            return null;
        }

        public async Task<List<UserWithTaskModel>> GetUserOrderByNameWithTask()
        {
            HttpResponseMessage response = await _httpClient.GetAsync($"/api/Queries/GetTeams");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                return JsonSerializer.Deserialize<List<UserWithTaskModel>>(i, options);
            }
            return null;
        }
        public async Task<List<TaskDTO>> GetAllTasks()
        {
            HttpResponseMessage response = await _httpClient.GetAsync($"/api/Tasks");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                return JsonSerializer.Deserialize<List<TaskDTO>>(i, options);
            }
            return null;
        }
        public async Task UpdateTask(TaskDTO task)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(task);
            HttpResponseMessage response = await _httpClient.PutAsync($"/api/Tasks", new StringContent(json, Encoding.UTF8, "application/json"));
        }
    }
}
