﻿using Microsoft.Extensions.DependencyInjection;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.MappingProfiles;
using Project_Structure.BLL.Services;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Repositories;
using System.Reflection;

namespace Project_Structure.API.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterCustomServices(this IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, EFUnitOfWork>();
            services.AddScoped<IQueryService, QueryService>();
            services.AddTransient<IProjectService, ProjectService>();
            services.AddTransient<ITaskService, TasksService>();
            services.AddTransient<ITeamService, TeamService>();
            services.AddTransient<IUserService, UserService>();

        }
        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
            },
            Assembly.GetExecutingAssembly());
        }
    }
}
