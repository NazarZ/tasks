﻿using Microsoft.AspNetCore.Mvc;
using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project_Structure.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private IProjectService _projectService;
        public ProjectsController(IProjectService service)
        {
            _projectService = service;
        }

        // GET: api/Projects
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectDTO>>> Get()
        {
            Response.ContentType = "application/json";
            return Ok(await _projectService.GetAllProjects());
        }

        // GET: api/Projects/id
        [HttpGet("{id}")]
        public async Task<ActionResult> Get(string id)
        {
            try
            {
                Response.ContentType = "application/json";
                var result = await _projectService.GetProjectById(Convert.ToInt32(id));
                return result is not null ? Ok(result) : NotFound();
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "400")
                {
                    return BadRequest(e.Message);
                }

                if (e.ParamName == "404")
                {
                    return NotFound(e.Message);
                }

                return NotFound(e.Message);
            }
        }
        // POST: api/Projects
        //create
        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> Post([FromBody] ProjectDTO project)
        {
            try
            {
                if (project is not null)
                {
                    await _projectService.CreateProject(project);
                    Response.ContentType = "application/json";
                    return Created("", project);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "400")
                {
                    return BadRequest(e.Message);
                }

                if (e.ParamName == "404")
                {
                    return NotFound(e.Message);
                }

                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        // PUT: api/Projects
        //Update
        [HttpPut]
        public async Task<ActionResult<ProjectDTO>> Put([FromBody] ProjectDTO project)
        {
            try
            {
                if (project is not null)
                {
                    await _projectService.UpdateProject(project);
                    Response.ContentType = "application/json";
                    return Created("", project);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "400")
                {
                    return BadRequest(e.Message);
                }

                if (e.ParamName == "404")
                {
                    return NotFound(e.Message);
                }

                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }



        // DELETE: api/Projects/id
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            try
            {
                Response.ContentType = "application/json";
                await _projectService.DeleteProject(Convert.ToInt32(id));
                return NoContent();
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "400")
                {
                    return BadRequest(e.Message);
                }

                if (e.ParamName == "404")
                {
                    return NotFound(e.Message);
                }

                return NotFound(e.Message);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
        }
    }



}
