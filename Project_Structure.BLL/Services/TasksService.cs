﻿using AutoMapper;
using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Services.Abstract;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Services
{
    public class TasksService : BaseService, ITaskService, IDisposable
    {
        public TasksService(IUnitOfWork context, IMapper mapper)
            : base(context, mapper)
        {

        }
        public void Dispose()
        {
            _context.Dispose();
        }
        public async Task<TaskDTO> CreateTask(TaskDTO task)
        {
            task.Id = 0;
            var taskEntity = _mapper.Map<DAL.Entities.Task>(task);
            await _context.Tasks.Create(taskEntity);
            await _context.SaveAsync();
            return _mapper.Map<TaskDTO>(taskEntity);
        }

        public async System.Threading.Tasks.Task DeleteTask(int idTask)
        {
            await _context.Tasks.Delete(idTask);
            await _context.SaveAsync();
        }

        public async Task<IEnumerable<TaskDTO>> GetAllTasks()
        {
            return _mapper.Map<List<TaskDTO>>(await _context.Tasks.GetAll());
        }

        public async Task<TaskDTO> GetTaskById(int id)
        {
            return _mapper.Map<TaskDTO>(await _context.Tasks.Get(id));
        }

        public async Task<TaskDTO> UpdateTask(TaskDTO task)
        {
            var taskEntity = _mapper.Map<DAL.Entities.Task>(task);
            await _context.Tasks.Update(taskEntity);
            await _context.SaveAsync();
            return _mapper.Map<TaskDTO>(taskEntity);
        }
    }
}
