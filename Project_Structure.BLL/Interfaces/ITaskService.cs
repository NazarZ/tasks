﻿using Project_Structure.BLL.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Interfaces
{
    public interface ITaskService
    {
        public Task<IEnumerable<TaskDTO>> GetAllTasks();
        public Task<TaskDTO> GetTaskById(int id);
        public Task<TaskDTO> CreateTask(TaskDTO task);
        public Task<TaskDTO> UpdateTask(TaskDTO task);
        public Task DeleteTask(int idTask);
    }
}
