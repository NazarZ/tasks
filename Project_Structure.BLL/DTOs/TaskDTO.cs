﻿using Binary.Linq.BL.Models.Enum;
using System;
using System.ComponentModel.DataAnnotations;

namespace Project_Structure.BLL.DTOs
{
    public class TaskDTO
    {
        public int Id { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "The field {0} must be greater than {1}.")]
        public int ProjectId { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "The field {0} must be greater than {1}.")]
        public int PerformerId { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public TaskState State { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }
        [DataType(DataType.Date)]
        public DateTime? FinishedAt { get; set; }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            TaskDTO otherTaskDTO = obj as TaskDTO;
            if (otherTaskDTO != null)
            {
                return this.Id.Equals(otherTaskDTO.Id);
            }
            else
            {
                throw new ArgumentException("Type of object isn't valid");
            }
        }
    }
}
