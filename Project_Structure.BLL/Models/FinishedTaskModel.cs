﻿namespace Project_Structure.BLL.Models
{
    public class FinishedTaskModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
