﻿using Project_Structure.DAL.Entities;
using System.Collections.Generic;


namespace Project_Structure.BLL.Models
{
    public class UserWithTaskModel
    {
        public User User { get; set; }
        public List<Task> Tasks { get; set; }
    }
}
