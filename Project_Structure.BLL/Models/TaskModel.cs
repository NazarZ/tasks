﻿using Project_Structure.DAL.Entities;

namespace Project_Structure.BLL.Models
{
    public class TaskModel
    {
        public Task Task { get; set; }
        public User Performer { get; set; }
    }
}
