﻿using Project_Structure.DAL.Entities;

namespace Project_Structure.BLL.Models
{
    public class Task6Model
    {
        public User User { get; set; }
        public Project LastProjectUser { get; set; }
        public int TotalTaskInLastProject { get; set; }
        public int TotalCountUnfinishedTask { get; set; }
        public Task LongestUserTask { get; set; }
    }
}
