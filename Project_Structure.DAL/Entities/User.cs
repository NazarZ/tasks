﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Project_Structure.DAL.Entities
{
    public class User : BaseEntity
    {

        [MaxLength(20)]
        public string? FirstName { get; set; }
        [MaxLength(20)]
        public string? LastName { get; set; }
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string? Email { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime RegisteredAt { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime BirthDay { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "The field {0} must be greater than {1}.")]
        public int? TeamId { get; set; }
        [JsonIgnore]
        public Team Team { get; set; }
    }
}
