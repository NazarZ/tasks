﻿using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using System.Collections.Generic;
using System.Reflection;
using System.Resources;
using System.Text.Json;

namespace Project_Structure.DAL.Data
{
    public static class Іnitializer
    {
        private static ResourceManager rm = new ResourceManager("Project_Structure.DAL.Resources", Assembly.GetExecutingAssembly());

        static JsonSerializerOptions options = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true
        };
        public static List<Project> ProjectInit()
        {
            var json = rm.GetString("ProjectsJson");
            return JsonSerializer.Deserialize<List<Project>>(json, options);
        }
        public static List<Task> TaskInit()
        {
            var json = rm.GetString("TasksJson");
            return JsonSerializer.Deserialize<List<Task>>(json, options);
        }
        public static List<Team> TeamInit()
        {
            var json = rm.GetString("TeamsJson");
            return JsonSerializer.Deserialize<List<Team>>(json, options);
        }
        public static List<User> UserInit()
        {
            var json = rm.GetString("UsersJson");
            return JsonSerializer.Deserialize<List<User>>(json, options);
        }
        public static void EFInit(IUnitOfWork db)
        {
            foreach (var project in ProjectInit())
            {
                db.Projects.Create(project);
            }
            foreach (var task in TaskInit())
            {
                db.Tasks.Create(task);
            }
            foreach (var team in TeamInit())
            {
                db.Teams.Create(team);
            }
            foreach (var user in UserInit())
            {
                db.Users.Create(user);
            }
        }
    }
}
