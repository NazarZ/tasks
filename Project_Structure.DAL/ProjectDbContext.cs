﻿using Microsoft.EntityFrameworkCore;
using Project_Structure.DAL.Data;
using Project_Structure.DAL.Entities;
using System;
using System.Linq;

namespace Project_Structure.DAL.Context
{
    public class ProjectDbContext : DbContext
    {
        public ProjectDbContext(DbContextOptions<ProjectDbContext> options)
            : base(options)
        {

        }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            Random r = new Random();
            modelBuilder.Entity<Team>().HasData(Іnitializer.TeamInit().Where(x => x.Id > 0).ToList());
            var users = Іnitializer.UserInit().Where(x => x.Id > 0).ToList();
            users.ForEach(item => { if (item.TeamId == 0 || item.TeamId > 9) item.TeamId = r.Next(1, 9); });
            modelBuilder.Entity<User>().HasData(users);
            var projects = Іnitializer.ProjectInit().Where(x => x.Id > 0).ToList();
            projects.ForEach(item => { if (item.TeamId == 0 || item.TeamId > 9) item.TeamId = r.Next(1, 9); });
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(Іnitializer.TaskInit().Where(x => x.Id > 0).ToList());

            base.OnModelCreating(modelBuilder);
        }
    }
}
