﻿using Project_Structure.DAL.Entities;
using System;

namespace Project_Structure.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Project> Projects { get; }
        IRepository<Task> Tasks { get; }
        IRepository<Team> Teams { get; }
        IRepository<User> Users { get; }
        void Save();
        System.Threading.Tasks.Task SaveAsync();
    }
}
