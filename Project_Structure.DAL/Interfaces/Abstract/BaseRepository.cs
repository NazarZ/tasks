﻿using Project_Structure.DAL.Context;

namespace Project_Structure.DAL.Interfaces.Abstract
{
    public abstract class BaseRepository
    {
        protected readonly ProjectDbContext _db;
        public BaseRepository(ProjectDbContext context)
        {
            _db = context;
        }
    }
}
