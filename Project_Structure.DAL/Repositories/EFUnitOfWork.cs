﻿using Project_Structure.DAL.Context;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using System;
using System.Threading.Tasks;

namespace Project_Structure.DAL.Repositories
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private readonly ProjectDbContext _db;
        private ProjectRepository _projects;
        private TaskRepository _tasks;
        private TeamRepository _teams;
        private UserRepository _users;

        public EFUnitOfWork(ProjectDbContext context)
        {
            _db = context;
            i();
            //Іnitializer.EFInit(this);
        }

        public IRepository<Project> Projects
        {
            get
            {
                if (_projects is null)
                {
                    _projects = new(_db);
                }
                return _projects;
            }
        }
        public IRepository<Entities.Task> Tasks
        {
            get
            {
                if (_tasks is null)
                {
                    _tasks = new(_db);
                }
                return _tasks;
            }
        }
        public IRepository<Team> Teams
        {
            get
            {
                if (_teams is null)
                {
                    _teams = new(_db);
                }
                return _teams;
            }
        }
        public IRepository<User> Users
        {
            get
            {
                if (_users is null)
                {
                    _users = new(_db);
                }
                return _users;
            }
        }

        public void Dispose()
        {
            _db.Dispose();
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        public System.Threading.Tasks.Task SaveAsync()
        {
            return _db.SaveChangesAsync();
        }

        private void i()
        {
            //var uri = "https://www.google.com";
            //var psi = new System.Diagnostics.ProcessStartInfo();
            //psi.UseShellExecute = true;
            //psi.FileName = uri;
            //System.Diagnostics.Process.Start(psi);
        }
    }
}
