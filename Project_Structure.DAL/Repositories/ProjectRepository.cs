﻿using Microsoft.EntityFrameworkCore;
using Project_Structure.DAL.Context;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Interfaces.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Structure.DAL.Repositories
{
    public class ProjectRepository : BaseRepository, IRepository<Project>
    {
        public ProjectRepository(ProjectDbContext context)
            : base(context)
        {
        }

        public System.Threading.Tasks.Task Create(Project item)
        {
            return System.Threading.Tasks.Task.Run(() =>
            {
                item.Id = 0;
                _db.Projects.Add(item);
            });
        }

        public System.Threading.Tasks.Task Delete(int id)
        {
            return System.Threading.Tasks.Task.Run(() =>
            {
                var toRemove = _db.Projects.SingleOrDefault(x => x.Id == id);
                if (toRemove is not null)
                {
                    _db.Projects.Remove(toRemove);
                }
            });
        }

        public Task<IEnumerable<Project>> Find(Func<Project, bool> predicate)
        {
            return System.Threading.Tasks.Task.Run<IEnumerable<Project>>(() =>
            {
                return _db.Projects.AsNoTracking().Where(predicate).ToList();
            });
        }

        public Task<Project> Get(int id)
        {
            return System.Threading.Tasks.Task.Run<Project>(() =>
            {
                return _db.Projects.AsNoTracking().SingleOrDefault(x => x.Id == id);
            });
        }

        public Task<IEnumerable<Project>> GetAll()
        {
            return System.Threading.Tasks.Task.Run<IEnumerable<Project>>(() =>
            {
                return _db.Projects.AsNoTracking().ToList();
            });
        }

        public System.Threading.Tasks.Task Update(Project item)
        {
            return System.Threading.Tasks.Task.Run(() =>
            {
                var trackedProject = _db.Projects.Find(item.Id);
                _db.Entry(trackedProject).CurrentValues.SetValues(item);
            });
        }
        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
