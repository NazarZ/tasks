﻿using Microsoft.EntityFrameworkCore;
using Project_Structure.DAL.Context;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Interfaces.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project_Structure.DAL.Repositories
{
    public class TeamRepository : BaseRepository, IRepository<Team>
    {
        public TeamRepository(ProjectDbContext context)
            : base(context)
        {
        }

        public System.Threading.Tasks.Task Create(Team item)
        {
            return System.Threading.Tasks.Task.Run(() =>
            {
                item.Id = 0;
                _db.Teams.Add(item);
            });
        }

        public System.Threading.Tasks.Task Delete(int id)
        {
            return System.Threading.Tasks.Task.Run(() =>
            {
                var toRemove = _db.Teams.SingleOrDefault(x => x.Id == id);
                if (toRemove is not null)
                {
                    _db.Teams.Remove(toRemove);
                }
            });
        }

        public System.Threading.Tasks.Task<IEnumerable<Team>> Find(Func<Team, bool> predicate)
        {
            return System.Threading.Tasks.Task.Run<IEnumerable<Team>>(() =>
            {
                return _db.Teams.AsNoTracking().Where(predicate).ToList();
            });

        }

        public System.Threading.Tasks.Task<Team> Get(int id)
        {
            return System.Threading.Tasks.Task.Run(() =>
            {
                return _db.Teams.AsNoTracking().SingleOrDefault(x => x.Id == id);
            });
        }

        public System.Threading.Tasks.Task<IEnumerable<Team>> GetAll()
        {
            return System.Threading.Tasks.Task.Run<IEnumerable<Team>>(() =>
            {
                return _db.Teams.AsNoTracking().ToList();
            });
        }

        public System.Threading.Tasks.Task Update(Team item)
        {
            return System.Threading.Tasks.Task.Run(() =>
            {
                var trackedTeam = _db.Teams.Find(item.Id);
                _db.Entry(trackedTeam).CurrentValues.SetValues(item);
            });
        }
        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
