﻿using Microsoft.EntityFrameworkCore;
using Project_Structure.DAL.Context;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Interfaces.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project_Structure.DAL.Repositories
{
    public class UserRepository : BaseRepository, IRepository<User>
    {
        public UserRepository(ProjectDbContext context)
            : base(context)
        {
        }

        public System.Threading.Tasks.Task Create(User item)
        {
            return System.Threading.Tasks.Task.Run(() =>
            {
                item.Id = 0;
                _db.Users.Add(item);
            });
        }

        public System.Threading.Tasks.Task Delete(int id)
        {
            return System.Threading.Tasks.Task.Run(() =>
            {
                var toRemove = _db.Users.SingleOrDefault(x => x.Id == id);
                if (toRemove is not null)
                {
                    _db.Users.Remove(toRemove);
                }
                else
                {
                    throw new ArgumentException("User does not exist", "404");
                }
            });
        }

        public System.Threading.Tasks.Task<IEnumerable<User>> Find(Func<User, bool> predicate)
        {
            return System.Threading.Tasks.Task.Run<IEnumerable<User>>(() =>
            {
                return _db.Users.AsNoTracking().Where(predicate).ToList();
            });
        }

        public System.Threading.Tasks.Task<User> Get(int id)
        {
            return System.Threading.Tasks.Task.Run<User>(() =>
            {
                return _db.Users.AsNoTracking().SingleOrDefault(x => x.Id == id);
            });
        }

        public System.Threading.Tasks.Task<IEnumerable<User>> GetAll()
        {
            return System.Threading.Tasks.Task.Run<IEnumerable<User>>(() =>
            {
                return _db.Users.AsNoTracking().ToList();
            });
        }

        public System.Threading.Tasks.Task Update(User item)
        {
            return System.Threading.Tasks.Task.Run(() =>
            {
                var trackedUser = _db.Users.Find(item.Id);
                _db.Entry(trackedUser).CurrentValues.SetValues(item);
            });
        }
        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
